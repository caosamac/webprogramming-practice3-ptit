-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Nov 30, 2021 at 03:08 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ltw_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `name`) VALUES
(1, 'nv1', 'nv1', 'Trang Thu');

-- --------------------------------------------------------

--
-- Table structure for table `registersubject`
--

CREATE TABLE `registersubject` (
  `id` int(11) NOT NULL,
  `pointCC` float NOT NULL,
  `pointKT` float NOT NULL,
  `pointExam` float NOT NULL,
  `createAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updateAt` timestamp NULL DEFAULT NULL,
  `Userid` int(11) NOT NULL,
  `SubjectSemesterid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `startYear` int(11) NOT NULL,
  `endYear` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updateAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `numberOfCreadits` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updateAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subjectsemester`
--

CREATE TABLE `subjectsemester` (
  `id` int(11) NOT NULL,
  `numberOfSlot` int(11) NOT NULL,
  `room` varchar(255) NOT NULL,
  `totalTime` int(11) NOT NULL,
  `examAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `roomExam` varchar(255) NOT NULL,
  `createAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updateAt` timestamp NULL DEFAULT NULL,
  `Subjectid` int(11) NOT NULL,
  `Semesterid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `studentID` varchar(20) NOT NULL,
  `classID` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `studentID`, `classID`, `gender`, `email`, `phone`, `address`, `photo`) VALUES
(1, 'Nguyễn Thu Trang', 'B18DCCN662', 'D18HTTT1', 'Nữ', 'trang@gmail.com', '0866018446', 'Hà Nội', 'https://bloganchoi.com/wp-content/uploads/2020/02/yeri-instagram3.jpg'),
(23, 'Hoàng Ngọc Hoa', 'B18DCCN212', 'D18HTTT2', 'Nữ', 'ndqk@gmail.com', '023642954', 'Ba Vì, Hà Nội', 'https://image.thanhnien.vn/660/uploaded/trucdl/2020_07_31/irenexinhdepchupquangcao4_qnei.png'),
(24, 'Trang Hoàng', 'B18DCCN124', 'D18CNPM8', 'Nữ', 'trangyuan28@gmail.com', '0464763768', 'HN', 'https://image.thanhnien.vn/660/uploaded/trucdl/2020_07_31/irenexinhdepchupquangcao4_qnei.png'),
(27, 'Hoàng Ngọc Trâm', 'B18DCAT101', 'D18ATTT2', 'Nữ', 'na.min.mit212@gmail.com', '04573456398', 'Ba Vì, Hà Nội', 'https://image.thanhnien.vn/660/uploaded/trucdl/2020_07_31/irenexinhdepchupquangcao4_qnei.png'),
(28, 'Chanyeol', 'B18DCAT112', 'D18HTTT2', 'Nam', 'test@gmail.com', '56545767', 'Ba Vì, Hà Nội', 'https://image.thanhnien.vn/660/uploaded/trucdl/2020_07_31/irenexinhdepchupquangcao4_qnei.png'),
(30, 'Mạn Đình', 'B18DCAT112', 'D18HTTT6', 'Nữ', 'ndqk@gmail.com', '525543636', 'Hòa Bình', 'https://image.thanhnien.vn/660/uploaded/trucdl/2020_07_31/irenexinhdepchupquangcao4_qnei.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `registersubject`
--
ALTER TABLE `registersubject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKRegisterSu448348` (`Userid`),
  ADD KEY `FKRegisterSu258659` (`SubjectSemesterid`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjectsemester`
--
ALTER TABLE `subjectsemester`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKSubjectSem10219` (`Subjectid`),
  ADD KEY `FKSubjectSem908135` (`Semesterid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registersubject`
--
ALTER TABLE `registersubject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjectsemester`
--
ALTER TABLE `subjectsemester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `registersubject`
--
ALTER TABLE `registersubject`
  ADD CONSTRAINT `FKRegisterSu258659` FOREIGN KEY (`SubjectSemesterid`) REFERENCES `subjectsemester` (`id`),
  ADD CONSTRAINT `FKRegisterSu448348` FOREIGN KEY (`Userid`) REFERENCES `user` (`id`);

--
-- Constraints for table `subjectsemester`
--
ALTER TABLE `subjectsemester`
  ADD CONSTRAINT `FKSubjectSem10219` FOREIGN KEY (`Subjectid`) REFERENCES `subject` (`id`),
  ADD CONSTRAINT `FKSubjectSem908135` FOREIGN KEY (`Semesterid`) REFERENCES `semester` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
